/**
 * Created by dmitrypopov on 11.10.15.
 */
var app = angular.module('converter', []);
app.controller('mainController', function ($scope) {
    $scope.convert = function () {
        if (IsJsonString($scope.inputData)) {
            $scope.outputData = Convert($scope.inputData)
        }
        else {
            alert('JSON не валиден!');
        }
    };
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    function Convert(src) {
        var inp = JSON.parse(src);
        var output = {};
        for (i = 0; i < inp.length; i++) {
            var item = inp[i];
            angular.forEach(item, function (value, key) {
                if (output[key] == undefined) {

                    output[key] = [];
                }
                if (value != "") {
                    output[key].push(value);
                }
            });
        }

        return JSON.stringify(output);
    }
});